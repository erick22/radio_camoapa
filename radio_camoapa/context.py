from multimedia.models import BannerPublicitarios
from radio.models import Articulos, SubCategorias
import feedparser
from datetime import datetime

def global_datos(request):
    # publicidad
    HOY_ES = datetime.now()
    base_header = BannerPublicitarios.objects.filter(donde = 1)
    base_footer = BannerPublicitarios.objects.filter(donde = 2)
    primer_anuncio = BannerPublicitarios.objects.filter(donde = 3)
    segundo_anuncio = BannerPublicitarios.objects.filter(donde = 4)
    quienes_somos_anuncio = BannerPublicitarios.objects.filter(donde = 5)
    noticias_destacadas = Articulos.objects.filter(aprobacion = 2,fecha__lte=HOY_ES).order_by('-id')[:6]
    #menus de producciones
    menu_produccion = SubCategorias.objects.filter(categoria__id=2)

    #rss
    feeds = feedparser.parse('https://agenciapulsar.org/feed/')
    feeds_hoy = feedparser.parse('https://www.hoy.com.ni/feed/')

    return {'base_header':base_header, 'base_footer':base_footer,
            'primer_anuncio':primer_anuncio, 'segundo_anuncio':segundo_anuncio,
            'quienes_somos_anuncio':quienes_somos_anuncio,
            'noticias_destacadas':noticias_destacadas,
            'menu_produccion':menu_produccion,
            'feeds':feeds,
            'feeds_hoy':feeds_hoy}
