# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from sorl.thumbnail import ImageField
from django.template.defaultfilters import slugify
from smart_selects.db_fields import ChainedForeignKey
from radio.utils import get_file_path
from django.contrib.auth.models import User
from tagging.fields import TagField

from multimedia.models import Galerias
from django.core.urlresolvers import reverse
import os

# Create your models here.
CHOICE_APROBACION = (
                        (1, 'Borrador'),
                        (2, 'Aprobado'),
                    )

CHOICE_SINO = (
                        (1, 'Si'),
                        (2, 'No'),
                    )

@python_2_unicode_compatible
class Categorias(models.Model):
    nombre = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, editable=False)

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.nombre))
        super(Categorias, self).save(*args, **kwargs)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "Categoria"
        verbose_name_plural = "Categorias"
        unique_together = ("nombre",)

@python_2_unicode_compatible
class SubCategorias(models.Model):
    categoria = models.ForeignKey(Categorias)
    subcategoria = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.subcategoria))
        super(SubCategorias, self).save(*args, **kwargs)

    def __str__(self):
        return u'%s' % (self.subcategoria)

    class Meta:
        verbose_name = "Sub Categoria"
        verbose_name_plural = "Sub Categorias"
        #unique_together = ("subcategoria",)

#change file name
def get_file_path_rename(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (instance.slug + '-1', ext)
    return os.path.join(instance.fileDir, filename)


@python_2_unicode_compatible
class Articulos(models.Model):
    titulo = models.CharField(max_length=450)
    slug = models.SlugField(max_length=450)
    fecha = models.DateTimeField()
    destacado =  models.IntegerField(choices=CHOICE_SINO)
    categoria = models.ForeignKey(Categorias)
    subcategoria = ChainedForeignKey(
        SubCategorias,
        chained_field="categoria",
        chained_model_field="categoria",
        show_all=False,
        auto_choose=True,
        null=True,
        blank=True
    )
    imagen_portada = ImageField('Foto principal', upload_to=get_file_path_rename, blank=True, null=True,help_text="Tamaño de la imagen 670x490")
    imagen_alterna = models.URLField(null=True, blank=True,
                                    help_text="Introduzca la url donde esta la imagen para ser colocada como portada")
    credito_foto_portada = models.CharField('Credito foto portada', max_length=250, null=True, blank=True)
    url = models.URLField('url del video como portada', blank=True, null=True)
    aprobacion = models.IntegerField(choices=CHOICE_APROBACION, default='1', verbose_name='Aprobación')
    contenido = RichTextUploadingField()
    tags = TagField('Palabras claves', help_text='Palabras separadas por coma (uno,dos,etc)', null=True)
    meta_description = models.TextField('Meta descripción',blank=True, null=True)

    # galeria = GenericRelation(Galerias)

    user = models.ForeignKey(User)

    fileDir = 'articulos/'

    # def save(self, *args, **kwargs):
    #     if not self.pk:
    #         self.slug = (slugify(self.titulo))
    #     super(Articulos, self).save(*args, **kwargs)

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = "Articulo"
        verbose_name_plural = "Articulos"
    
    def get_absolute_url(self):
        return reverse('noticia-detalle',args=[str(self.subcategoria.slug),str(self.slug)])


from solo.models import SingletonModel

class Configuracion(SingletonModel):
    html_audio = models.TextField(blank=True, null=True)
    html_en_vivo = models.TextField(blank=True, null=True)

    def __str__(self):
        return "Configuracion"

    class Meta:
        verbose_name = "Configuracion"
