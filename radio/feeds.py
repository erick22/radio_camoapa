# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from radio.models import Articulos

class LatestArticulosFeed(Feed):
    title = "Radio Camoapa"
    link = "/noticias/"
    description = "Actualización de las ultimas noticias de la radio camoapa"

    def items(self):
        return Articulos.objects.order_by('-fecha')[:6]

    def item_title(self, item):
        return item.titulo

    def item_description(self, item):
        return item.contenido

    # item_link is only needed if Revistas has no get_absolute_url method.
    def item_link(self, item):
        return reverse('noticia-detalle', args=[item.pk, item.slug])
