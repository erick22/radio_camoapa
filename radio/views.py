# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from radio.models import *
from multimedia.models import *
from datetime import datetime
from itertools import chain
from radio.forms import *
from django.template.loader import render_to_string
from django.core.mail import send_mail, EmailMultiAlternatives
import time
from datetime import datetime
from parrila_programacion.models import *
import locale
from django.db.models import Q, F
from django.db.models.functions import Now
# Create your views here.

class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        #articulos principales
        HOY_ES = datetime.now()
        # noticias_locales = Articulos.objects.filter(categoria_id=1,subcategoria_id=7,aprobacion=2,fecha__lte=HOY_ES).order_by('-fecha').select_related().values_list('imagen_portada','url','imagen_alterna','categoria__nombre','subcategoria__subcategoria','contenido','titulo','fecha')[:1]
        # # Articulos.objects.filter(categoria=1, subcategoria=7,aprobacion=2,fecha__lte=HOY_ES).select_related('categoria').order_by('-fecha')[:1]
        # noticias_nacionales = Articulos.objects.filter(categoria_id=1,subcategoria_id=1,aprobacion=2,fecha__lte=HOY_ES).order_by('-fecha').select_related().values_list('imagen_portada','url','imagen_alterna','categoria__nombre','subcategoria__subcategoria','contenido','titulo','fecha')[:1]
        # # Articulos.objects.filter(categoria=1, subcategoria=1,aprobacion=2,fecha__lte=HOY_ES).select_related('categoria').order_by('-fecha')[:1]
        # noticias_internacionales = Articulos.objects.filter(categoria_id=1,subcategoria_id=2,aprobacion=2,fecha__lte=HOY_ES).order_by('-fecha').select_related().values_list('imagen_portada','url','imagen_alterna','categoria__nombre','subcategoria__subcategoria','contenido','titulo','fecha')[:1]
        # # Articulos.objects.filter(categoria=1, subcategoria=2,aprobacion=2,fecha__lte=HOY_ES).select_related('categoria').order_by('-fecha')[:1]
        # art_principal = list(chain(noticias_locales, noticias_nacionales, noticias_internacionales))

        # ultimas_cat = Articulos.objects.filter(categoria=1,aprobacion=2,fecha__lte=HOY_ES).select_related('categoria').values_list('subcategoria').distinct('subcategoria').order_by('subcategoria')[:6]
        dict = {}
        for value in SubCategorias.objects.filter(categoria=1).select_related('categoria').order_by('subcategoria'):
            articulos = Articulos.objects.filter(categoria=1,subcategoria = value.id,aprobacion=2,fecha__lte=HOY_ES).select_related('categoria').order_by('-fecha')[:2]
            if  articulos:
                dict[value.subcategoria] = articulos

        # context['principal'] = art_principal

        #ultimas noticias
        ultimas_notas = Articulos.objects.filter(fecha__lte=HOY_ES).order_by('-fecha').select_related('categoria','subcategoria')
        context['ultimas_noticias'] = ultimas_notas[:9]
        context['ultimas'] = Articulos.objects.filter(destacado=1,fecha__lte=HOY_ES).distinct('subcategoria').order_by('subcategoria','-fecha').exclude(titulo = ultimas_notas.first())[:4]
        context['patrocinador'] = Patrocinadores.objects.all()
        context['galerias'] = Galerias.objects.filter(fecha__lte=HOY_ES,aprobado=1).order_by('-id')[:6]
        #
        context['notas_subcategorias'] = dict

        #programacion de la hora y el dia
        hoy = datetime.today()
        tupla_diassem = ("lunes", "martes", "miércoles", "jueves","viernes", "sábado","domingo")
        tupla_valores = datetime.isocalendar(hoy)
        dia = 0
        for value in DIA_CHOICES:
            if tupla_diassem[tupla_valores[2]-1].encode('utf-8') in value[1].lower().encode('utf-8'):
                dia = value[0]
        ###########

        context['programacion'] = ProgramacionDia.objects.filter(programacion__dia = dia)
        context['now'] = datetime.now().time()
        return context

def noticias_por_categoria(request, template='radio/articulos_list.html'):
    HOY_ES = datetime.now()
    noticias_locales = Articulos.objects.filter(categoria__slug='noticias', subcategoria__slug='locales',aprobacion=2,fecha__lte=HOY_ES).order_by('-fecha')[:4]
    noticias_nacionales = Articulos.objects.filter(categoria__slug='noticias', subcategoria__slug='nacionales',aprobacion=2,fecha__lte=HOY_ES).order_by('-fecha')[:4]
    noticias_internacionales = Articulos.objects.filter(categoria__slug='noticias', subcategoria__slug='internacionales',aprobacion=2,fecha__lte=HOY_ES).order_by('-fecha')[:4]

    print(noticias_locales)

    return render(request, template, locals())

def noticias_locales(request, slug='None', template='radio/lista_noticias.html'):
    HOY_ES = datetime.now()
    tipo = slug
    noticias = Articulos.objects.filter(categoria__slug='noticias',subcategoria__slug=slug, aprobacion=2,fecha__lte=HOY_ES).order_by('-fecha')
    object_list = noticias

    return render(request, template, locals())

class ListNoticias(ListView):
    model = Articulos
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(ListNoticias, self).get_context_data(**kwargs)
        context['noticias_destacadas'] = Articulos.objects.order_by('-id')[:6]
        return context

def DetailNoticias(request,subcategoria=None,slug=None):
    template = "detalle_noticia.html"
    object = Articulos.objects.get(subcategoria__slug = subcategoria,slug = slug)
    notas_relacionadas = Articulos.objects.filter(subcategoria__slug = subcategoria).exclude(id = object.id).order_by('-fecha')[:6]
    fotos = Fotos.objects.filter(object_id = object.id)

    return render(request, template, locals())

class GaleriaDetailView(DetailView):
    model = Galerias
    template_name = "galeria_detail.html"

    def get_context_data(self, **kwargs):
        HOY_ES = datetime.now()
        context = super(GaleriaDetailView, self).get_context_data(**kwargs)
        context['galerias_relacionadas'] = Galerias.objects.filter(fecha__lte=HOY_ES,tags__in=self.object.tags,aprobado=1).exclude(id=self.object.id).order_by('-fecha')[:6]
        return context

class ContactoView(TemplateView):
    template_name = "contacto.html"

    def get_context_data(self, **kwargs):
        context = super(ContactoView, self).get_context_data(**kwargs)

        #staff
        context['prensa'] = StaffRadio.objects.filter(area = '1')
        context['admon'] = StaffRadio.objects.filter(area = '2')
        context['produccion'] = StaffRadio.objects.filter(area = '3')
        context['locucion'] = StaffRadio.objects.filter(area = '4')

        return context

def contacto(request, template="contacto.html"):
    #staff
    prensa = StaffRadio.objects.filter(area = '1')
    admon = StaffRadio.objects.filter(area = '2')
    produccion = StaffRadio.objects.filter(area = '3')
    locucion = StaffRadio.objects.filter(area = '4')

    arreglo_mail = ['info@radiocamoapa.com',]
    if request.method == 'POST':
        form = EmailForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            try:
                text_content = render_to_string('notify.txt',{'name': name,'email': email,'message':message})
                send_mail('Web Radio Camoapa', text_content, 'noreply@radiocamoapa.com', arreglo_mail)

                enviado = 1

            except:
                pass
        else:
			enviado = 0

    else:
        form = EmailForm()

    return render(request, template, locals())

def lista_producciones(request, template='radio/producciones.html'):
    HOY_ES = datetime.now()
    todo_prod = Articulos.objects.filter(categoria__slug='producciones', aprobacion=2,fecha__lte=HOY_ES).order_by('-fecha')
    object_list = todo_prod

    return render(request, template, locals())

def produccion_locales(request, slug='None', template='radio/producciones.html'):
    tipo = slug
    HOY_ES = datetime.now()
    noticias = Articulos.objects.filter(categoria__slug='producciones',subcategoria__slug=slug, aprobacion=2,fecha__lte=HOY_ES).order_by('-fecha')
    object_list = noticias

    return render(request, template, locals())

def DetailProduccion(request,subcategoria=None,slug=None):
    template = "detalle_produccion.html"
    #object = Articulos.objects.filter(categoria__slug='producciones', aprobacion=2,fecha__lte=HOY_ES).order_by('-fecha')
    object = Articulos.objects.get(subcategoria__slug = subcategoria,slug = slug)
    notas_relacionadas = Articulos.objects.filter(subcategoria__slug = subcategoria).exclude(id = object.id).order_by('-fecha')[:6]
    fotos = Fotos.objects.filter(object_id = object.id)

    return render(request, template, locals())
