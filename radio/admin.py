from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline
from .models import *
from multimedia.models import Fotos
from django.contrib.flatpages.models import FlatPage
# Note: we are renaming the original Admin and Form as we import them!
from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld
from import_export.admin import ImportExportModelAdmin

from django import forms
from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget

class FlatpageForm(FlatpageFormOld):
    content = forms.CharField(widget=CKEditorUploadingWidget())
    class Meta:
        model = FlatPage # this is not automatically inherited from FlatpageFormOld
        fields = '__all__'


class FlatPageAdmin(FlatPageAdminOld):
    form = FlatpageForm

class FotosInline(GenericTabularInline):
    model = Fotos
    extra = 1

class ArticulosAdmin(ImportExportModelAdmin):
    inlines = [FotosInline]
    list_display = ['titulo', 'fecha','destacado','categoria','subcategoria','aprobacion']
    list_filter = ['fecha','destacado','categoria','subcategoria','aprobacion']
    search_fields = ['titulo',]
    date_hierarchy = 'fecha'
    prepopulated_fields = {"slug": ("titulo",)}

    # def add_view(self, request, form_url='', extra_context=None):
    #     self.readonly_fields = ''
    #     return super(ArticulosAdmin, self).add_view(request)

    # def change_view(self, request, object_id, form_url='', extra_context=None):
    #     self.fields = ['titulo','slug', 'fecha','destacado','categoria','subcategoria','aprobacion']
    #     return super(ArticulosAdmin, self).change_view(request,object_id)
    

class SubCategoriaAdmin(admin.ModelAdmin):
    list_display = ['categoria', 'subcategoria']

# Register your models here.
admin.site.register(Categorias)
admin.site.register(SubCategorias, SubCategoriaAdmin)
admin.site.register(Articulos, ArticulosAdmin)
#FlatPages
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)

from solo.admin import SingletonModelAdmin
admin.site.register(Configuracion, SingletonModelAdmin)
