# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-09-10 04:17
from __future__ import unicode_literals

import ckeditor.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import radio.utils
import sorl.thumbnail.fields
import tagging.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Articulos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=450)),
                ('slug', models.SlugField(editable=False, max_length=450)),
                ('fecha', models.DateTimeField()),
                ('destacado', models.IntegerField(choices=[(1, 'Si'), (2, 'No')])),
                ('imagen_portada', sorl.thumbnail.fields.ImageField(blank=True, null=True, upload_to=radio.utils.get_file_path, verbose_name='Foto principal')),
                ('url', models.URLField(blank=True, null=True, verbose_name='url del video como portada')),
                ('aprobacion', models.IntegerField(choices=[(1, 'Borrador'), (2, 'Aprobado')], default='1', verbose_name='Aprobaci\xf3n')),
                ('contenido', ckeditor.fields.RichTextField()),
                ('tags', tagging.fields.TagField(blank=True, help_text='Palabras separadas por coma (uno,dos,etc)', max_length=255, null=True, verbose_name='Etiquetas')),
            ],
            options={
                'verbose_name': 'Articulo',
                'verbose_name_plural': 'Articulos',
            },
        ),
        migrations.CreateModel(
            name='Categorias',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
                ('slug', models.SlugField(max_length=250)),
            ],
            options={
                'verbose_name': 'Categoria',
                'verbose_name_plural': 'Categorias',
            },
        ),
        migrations.CreateModel(
            name='SubCategorias',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subcategoria', models.CharField(max_length=250)),
                ('categoria', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='radio.Categorias')),
            ],
            options={
                'verbose_name': 'Sub Categoria',
                'verbose_name_plural': 'Sub Categorias',
            },
        ),
        migrations.AlterUniqueTogether(
            name='categorias',
            unique_together=set([('nombre',)]),
        ),
        migrations.AddField(
            model_name='articulos',
            name='categoria',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='radio.Categorias'),
        ),
        migrations.AddField(
            model_name='articulos',
            name='subcategoria',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='radio.SubCategorias'),
        ),
        migrations.AddField(
            model_name='articulos',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='subcategorias',
            unique_together=set([('subcategoria',)]),
        ),
    ]
