from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline
from .models import *

class FotosInline(GenericTabularInline):
    model = Fotos
    extra = 1

class GaleriaAdmin(admin.ModelAdmin):
    inlines = [FotosInline]

# Register your models here.
admin.site.register(Galerias, GaleriaAdmin)
admin.site.register(Patrocinadores)
admin.site.register(StaffRadio)
admin.site.register(BannerPublicitarios)
