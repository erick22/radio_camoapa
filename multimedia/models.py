# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.template.defaultfilters import slugify
from sorl.thumbnail import ImageField
from tagging.fields import TagField
from radio.utils import get_file_path
import os

# Create your models here.

#change file name
def get_file_path_rename(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (instance.content_object.slug + '-'+ str(instance.object_id), ext)
    return os.path.join(instance.fileDir, filename)

@python_2_unicode_compatible
class Fotos(models.Model):
    ''' Modelo generico para subir imagenes en todos los demas app :)'''
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.IntegerField(db_index=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    titulo = models.CharField(max_length=250, null=True, blank=True)
    foto = ImageField("Foto",upload_to=get_file_path_rename, null=True, blank=True,help_text="Tamaño de la imagen 370x310")
    foto_url = models.URLField(null=True, blank=True,
                                    help_text="Introduzca la url donde esta la imagen")
    tags = TagField('Etiquetas', help_text='Separar elementos con "," ', null=True)
    texto = models.CharField('Texto opcional', max_length=500, null=True, blank=True)

    fileDir = 'galerias/'

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = "Galeria"
        verbose_name_plural = "Galerias"


@python_2_unicode_compatible
class Patrocinadores(models.Model):
    nombre = models.CharField(max_length=250)
    siglas = models.CharField(max_length=50, null=True, blank=True)
    logo = ImageField("logo",upload_to=get_file_path, null=True, blank=True,help_text="Tamaño de la imagen 300x300")
    url = models.URLField('Sitio web', null=True, blank=True)
    contacto = models.CharField(max_length=250, null=True, blank=True)
    resumen = models.TextField()

    fileDir = 'patrocinadores/'

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "Socio y aliado"
        verbose_name_plural = "Socios y aliados"
        unique_together = ("nombre", "siglas")

CHOICE_GALERIA = (
            (1, 'Patrocinadores'),
            (2, 'Noticias'),
            (3, 'Otros'),
    )

CHOICE_GALERIA_PUBLICADA = (
            (1, 'Si'),
            (2, 'No'),
    )

@python_2_unicode_compatible
class Galerias(models.Model):
    titulo = models.CharField('Titulo de la galeria', max_length=250)
    aprobado =models.IntegerField(null=True, blank=True,choices=CHOICE_GALERIA_PUBLICADA)
    slug = models.SlugField(max_length=250, editable=False, null=True, blank=True)
    fecha = models.DateField()
    tipo = models.IntegerField(choices=CHOICE_GALERIA, null=True, blank=True)
    portada = ImageField("Portada",upload_to=get_file_path, null=True, blank=True,help_text="Tamaño de la imagen 670x490")
    portada_alterna = models.URLField(null=True, blank=True,
                                    help_text="Introduzca la url donde esta la imagen para ser colocada como portada")
    resumen = models.TextField()
    tags = TagField('Etiquetas', help_text='Separar elementos con "," ', null=True)
    galeria = GenericRelation(Fotos)

    fileDir = 'portadaGaleria/'

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.titulo))
        super(Galerias, self).save(*args, **kwargs)

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = "Galeria"
        verbose_name_plural = "Galerias"

CHOICES_CLASIFICACION = (
            (1, 'Prensa'),
            (2, 'Administración'),
            (3, 'Producción'),
            (4, 'Locución'),
        )

@python_2_unicode_compatible
class StaffRadio(models.Model):
    nombre = models.CharField('Nombres y apellidos', max_length=250)
    cargo = models.CharField(max_length=250)
    foto = ImageField("Foto",upload_to=get_file_path, null=True, blank=True,help_text="Tamaño de la imagen 270x270")
    area = models.IntegerField(choices=CHOICES_CLASIFICACION, null=True, blank=True)

    fileDir = 'staff/'

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Staff de la radio'
        verbose_name_plural = 'Staff de la radio'

CHOICE_LUGAR = (
        (1, 'base header'),
        (2, 'base footer'),
        (3, 'noticias primer anuncio'),
        (4, 'noticias ultimo anuncio'),
        (5, 'quienes somos anuncio'),
    )
@python_2_unicode_compatible
class BannerPublicitarios(models.Model):
    donde = models.IntegerField(choices=CHOICE_LUGAR)
    banner = ImageField("Banner",upload_to=get_file_path,help_text="Tamaño de la imagen 600x80")

    fileDir = 'banner/'

    def __str__(self):
        return self.get_donde_display()

    class Meta:
        verbose_name = 'Banner publicitario'
        verbose_name_plural = 'Banner publicitarios'
