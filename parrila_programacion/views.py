from django.shortcuts import render
from .models import *
from datetime import datetime

# Create your views here.

def programacion(request,template='radio/programacion.html'):
    object_list = Programacion.objects.order_by('dia')
    now = datetime.now().time()
    day = datetime.now()
    return render(request, template, locals())
